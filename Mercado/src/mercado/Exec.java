/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

import java.util.Scanner;
/**
 *
 * @author pedro
 */
public class Exec {
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner scanner = new Scanner(System.in);
        String codigo;
        String nome;
        int quantidade;
        double preco;
        System.out.println("Cadastro de Cliente.");
        Mercado m = new Mercado();
        while(true){
            
            System.out.println("Digite o nome do cliente: ");
            nome = scanner.nextLine();
            System.out.println("Digite o código do cliente: ");
            codigo = scanner.nextLine();
            
            if(codigo.equals("")){
                break;
            }
            Cliente c = new Cliente(nome, codigo);
            m.AdicionaCliente(c);
        }
        //System.out.println("Saiuuu");
        
        while(true){
            
            System.out.println("Digite o nome do produto: ");
            nome = scanner.nextLine();
            System.out.println("Digite o preco do produto: ");
            preco = Double.parseDouble(scanner.nextLine());
            System.out.println("Digite o codigo do produto: ");
            codigo = scanner.nextLine();
            
            
            
            if(codigo.equals("")){
                break;
            }
            Produto p = new Produto(nome, preco, codigo);
            m.AdicionaProduto(p);
        }
        
        System.out.println("Iniciando a venda.");
        
        System.out.println("Digite o codigo do cliente: ");
        codigo = scanner.nextLine();
        
        Cliente c;
        //try{
            c = m.ChecaCliente(codigo);
        //}catch(){
        
       //}
        
        if(c != null){
            Venda v = new Venda(m, c);
        
            while(true){
                System.out.println("Digite o codigo do produto a adicionar a venda: ");
                codigo = scanner.nextLine();
                
                
                
                if(codigo.equals("")){
                    break;
                }
                System.out.println("Digite a quantidade: ");
                quantidade = Integer.parseInt(scanner.nextLine());
                v.AdicionaItemAVenda(codigo, quantidade);
            }
            
            for(ItemVenda i: v.getItens()){
                System.out.println("COD: " +i.getProduto().getCodigo() + " Produto: " + i.getProduto().getNome() +  " Quant: " + i.getQuantidade() + " Preco Uni: " + i.getProduto().getPreco() + " Preco Total: " + i.getProduto().getPreco() * i.getQuantidade());
            }
            
            System.out.println("Valor total: "+ v.ValorTotal());
        }
        
        
        
    }
}
