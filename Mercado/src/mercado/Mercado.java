/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class Mercado {
    
    private ArrayList<Cliente> clientes;
    private ArrayList<Produto> produtos;
    
    /**
     * Instancia um novo Objeto Mercado.
     */
    public Mercado(){
        this.clientes =  new ArrayList<>();
        this.produtos = new ArrayList<>();
    }
    
    /**
     * Adiciona um cliente a Lista de clientes.
     * @param cliente 
     * 
     */
    public void AdicionaCliente(Cliente cliente){
        this.clientes.add(cliente);
    }
    
    /**
     * Remove um cliente da Lista de Clientes.
     * @param cliente 
     */
    public void RemoveCliente(Cliente cliente){
        this.clientes.remove(cliente);
    }

    /**
     * Retorna a Lista de clientes do mercado.
     * @return 
     */
    public ArrayList<Cliente> getClientes() {
        return this.clientes;
    }

    /**
     * Seta uma Lista de clientes.
     * @param clientes 
     */
    public  void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

    /**
     * Retorna a Lista de Produtos.
     * @return 
     */
    public ArrayList<Produto> getProdutos() {
        return this.produtos;
    }

     /**
      * Seta a Lista de Produtos.
      * @param produtos 
      */
    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    /**
     * Adiciona Produto a Lista de Produtos.
     * @param produto 
     */
    public void AdicionaProduto(Produto produto){
        this.produtos.add(produto);
    }
    
    /**
     * Remove Produto da Lista de Produtos.
     * @param produto 
     */
    public void RemoveProduto(Produto produto){
        this.produtos.remove(produto);
    }
    
    /**
     * Checa se há cliente com o código informado.
     * @param codigo
     * @return 
     */
    public Cliente ChecaCliente(String codigo){
        for(Cliente c: this.clientes){
                if(c.getCodigo().equals(codigo)){
                    return c;
                }
        }
        return null;
    }
        
}
