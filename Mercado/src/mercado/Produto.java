/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

/**
 *
 * @author pedro
 */
public class Produto {
    private String nome;
    private String codigo;
    private double preco;

    public Produto(String nome, double preco, String codigo){
        this.nome = nome;
        this.preco = preco;
        this.codigo = codigo;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Retorna o nome do produto.
     * @return 
     */
    public String getNome() {
        return nome;
    }

    /**
     * Seta um novo nome pro Produto.
     * @param nome 
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    /**
     * Retorna o Preço do produto.
     * @return 
     */
    public double getPreco() {
        return preco;
    }
    
    
    /**
     * Seta um novo preço pro produto.
     * @param preco 
     */
    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    
}
