/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

/**
 *
 * @author pedro
 */
public class ItemVenda {
    private Produto produto;
    private int quantidade;
    
    /**
     * 
     * @param produto Produto a ser adicionado ao ItemVenda.
     */
    public ItemVenda(Produto produto){
        this.produto = produto;
        this.quantidade = 1;
    }
    
    /**
     * 
     * @param produto Produto a ser adicionado ao ItemVenda[String].
     * @param quantidade Quantidade do produtoa ser adicionado ao ItemVenda[int].
     */
    public ItemVenda(Produto produto, int quantidade){
        this.produto = produto;
        this.quantidade = quantidade;
    }

    /**
     * 
     * @return Produto do item venda[Produto].
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * 
     * @param produto Produto do ItemVenda[Produto].
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * 
     * @return Quantidade de itens[int].
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * Seta a quantidade do produto[int].
     * @param quantidade 
     * 
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    
}
