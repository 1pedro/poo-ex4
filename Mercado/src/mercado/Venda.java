/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

import java.util.ArrayList;
/**
 *
 * @author pedro
 */
public class Venda {
    private Mercado mercado;
    private Cliente cliente;
    private ArrayList<ItemVenda> itens;
    
    public Venda(Mercado mercado, Cliente cliente){
        this.mercado = mercado;
        this.cliente = cliente;
        this.itens = new ArrayList<>();
    }
    
    /**
     * Adiciona um item a venda caso ele esteja em Mercado.
     * @param codigo
     * @param quantidade 
     */
    public void AdicionaItemAVenda(String codigo, int quantidade){
        Produto p1 = null;
        
        for(Produto p: this.mercado.getProdutos()){
            if(p.getNome().equals(codigo)){
                p1  = p;
            }
        }
        
        if(p1 != null){
            if(quantidade == 0){
                ItemVenda item = new ItemVenda(p1, 1);
                this.itens.add(item);
            }else{
                ItemVenda item = new ItemVenda(p1, quantidade);
                this.itens.add(item);
            }
        }
    }
    
    /**
     * Remove um produto da lista de produtos pelo Codigo do produto.
     * @param codigo 
     */
    public void RemoveItemAVenda(String codigo){

        for(ItemVenda item : this.itens){
            if(item.getProduto().getNome().equals(codigo)){
                this.itens.remove(item);
            }
        }
    }
    
    /**
     * Retorna o valor total dos itens.
     * @return 
     */
    public double ValorTotal(){
        double total = 0;
        for(ItemVenda item: this.itens){
            total += item.getProduto().getPreco();
        }
        
        return total;
    }
    
    /**
     * Retorna um objeto do tipo Mercado.
     * @return 
     */
    public Mercado getMercado() {
        return mercado;
    }
    
    /**
     * Seta um objeto do tipo mercado.
     * @param mercado 
     */
    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    /**
     * Retorna o Cliente.
     * @return 
     */
    public Cliente getCliente() {
        return cliente;
    }
    
    /**
     * Seta um Cliente.
     * @param cliente 
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Retorna itens da venda.
     * @return 
     */
    public ArrayList<ItemVenda> getItens() {
        return itens;
    }

    /**
     * Seta itens da venda.
     * @param itens 
     */
    public void setItens(ArrayList<ItemVenda> itens) {
        this.itens = itens;
    }
    
    
}
