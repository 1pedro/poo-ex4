/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercado;

/**
 *
 * @author pedro
 */
public class Cliente {
    private String nome;
    private String codigo;
    /**
     * 
     * @param nome Nome do Cliente[String]
     * @param codigo Codigo do Produto[String]
     */
    public Cliente(String nome, String codigo){
        this.nome = nome;
        this.codigo = codigo;
    }

    /**
     * @return Nome do cliente[String]
     */
    public String getNome() {
        return nome;
    }

    /**
     * Adiciona uma String como nome do cliente.
     * @param nome 
     * 
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * 
     * @return Codigo do cliente[String].
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Seta String como codigo do cliente.
     * @param codigo
     * 
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
